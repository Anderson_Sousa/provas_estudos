package com.calculo.impl.basico;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class RaizQuadrada implements ICalculadora {

	private ICalculadora iCalculadora;
	public RaizQuadrada(ICalculadora iCalculadora){
		this.iCalculadora = iCalculadora;
	}

	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		BigDecimal resultado = BigDecimal.ZERO;
		if(tipoCalculo.equals(RAIZ_QUADRADA)){
			return BigDecimal.valueOf(Math.sqrt(valor[0].doubleValue()));
		}
		if(iCalculadora == null){
			return resultado;
		}
		return iCalculadora.calcular(valor, tipoCalculo);
	}

}
