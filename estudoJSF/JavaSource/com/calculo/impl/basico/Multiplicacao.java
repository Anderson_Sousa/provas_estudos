package com.calculo.impl.basico;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Multiplicacao implements ICalculadora {

	private ICalculadora iCalculadora;
	public Multiplicacao(ICalculadora iCalculadora){
		this.iCalculadora = iCalculadora;
	}
	
	
	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		BigDecimal resultado = valor[0];
		if(tipoCalculo.equals(MULTIPLICACAO)){
			for (int i = 1; i < valor.length; i++) {
				resultado = resultado.multiply(valor[i]);
			}
			return resultado;
		}
		if(iCalculadora == null){
			return resultado;
		}
		return iCalculadora.calcular(valor, tipoCalculo);
	}

}
