package com.calculo.impl.basico;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Divisao implements ICalculadora {

	private ICalculadora iCalculadora;
	public Divisao(ICalculadora iCalculadora){
		this.iCalculadora = iCalculadora;
	}
	
	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		BigDecimal resultado = valor[0];
		if(tipoCalculo.equals(DIVISAO)){
			if(resultado.equals(new BigDecimal("0"))){
				return resultado;
			}
			for (int i = 1; i < valor.length; i++) {
				Double valorTemp = resultado.doubleValue();
				valorTemp = valorTemp / valor[i].doubleValue();
				resultado = BigDecimal.valueOf(valorTemp);
			}
			return resultado;
		}
		if(iCalculadora == null){
			return resultado;
		}
		return iCalculadora.calcular(valor, tipoCalculo);
	}

}
