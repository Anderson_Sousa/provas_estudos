package com.calculo.impl.basico;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Percentual implements ICalculadora{

	private ICalculadora iCalculadora;
	public Percentual(ICalculadora iCalculadora){
		this.iCalculadora = iCalculadora;
	}

	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		BigDecimal resultado = valor[0];
		if(tipoCalculo.equals(PERCENTUAL)){
			BigDecimal valorPercentual = valor[1];
			resultado = resultado.multiply((valorPercentual.divide(BigDecimal.valueOf(100l))));
			return resultado;
		}
		
		if(iCalculadora == null){
			return resultado;
		}
		return iCalculadora.calcular(valor, tipoCalculo);
	}

}
