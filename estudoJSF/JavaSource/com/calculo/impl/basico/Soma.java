package com.calculo.impl.basico;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Soma implements ICalculadora {

	private ICalculadora iCalculadora;
	public Soma(ICalculadora iCalculadora){
		this.iCalculadora = iCalculadora;
	}
	
	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		BigDecimal resultado = BigDecimal.ZERO;
		if(tipoCalculo.equals(SOMA)){
			for (int i = 0; i < valor.length; i++) {
				resultado = resultado.add(valor[i]);
			}
			return resultado;
		}
		if(iCalculadora == null){
			return resultado;
		}
		return iCalculadora.calcular(valor, tipoCalculo);
	}

}
