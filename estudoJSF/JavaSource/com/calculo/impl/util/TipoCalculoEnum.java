package com.calculo.impl.util;

public enum TipoCalculoEnum {

	SOMA(1, "+"),
	SUBTRACAO(2, "-"),
	DIVISAO(3, "/"),
	MULTIPLICACAO(4, "*"),
	PERCENTUAL(5, "%"),
	RAIZ_QUADRADA(6, "srqt"),
	DISTANCIA(7, "distancia"),
	PESO(8, "peso"),
	TEMPO(9, "tempo"),
	TEMPERATURA(10, "temperatura"),
	DIGITOS(11, "digito"),
	MOEDA(12, "real", "dolar");
	
	private Integer numero;
	private String descricao1;
	private String descricao2;
	
	TipoCalculoEnum(Integer numero, String descricao1){
		this.numero = numero;
		this.descricao1 = descricao1;
	}
	
	TipoCalculoEnum(Integer numero, String descricao1, String descricao2){
		this.numero = numero;
		this.descricao1 = descricao1;
		this.descricao2 = descricao2;
	}
	public static String getInfo(Integer numero){
		for (TipoCalculoEnum tipo : TipoCalculoEnum.values()) {
			if(tipo.getNumero().equals(numero)){
				return tipo.getDescricao1();
			}
		}
		return "";
	}
	public Integer getNumero(){
		return numero;
	}
	public String getDescricao1(){
		return descricao1;
	}
	public String getDescricao2(){
		return descricao2;
	}
}
