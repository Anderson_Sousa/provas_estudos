package com.calculo.impl.conversao.distancia;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Milimetro implements ICalculadora {


	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		return valor[0].multiply(BigDecimal.valueOf(10l));
	}

}
