package com.calculo.impl.conversao.distancia;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Distancia implements ICalculadora {

	private ICalculadora iCalculadora;
	public Distancia(ICalculadora iCalculadora){
		this.iCalculadora = iCalculadora;
	}

	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		
		if(tipoCalculo.equals(DISTANCIA)){
			return iCalculadora.calcular(valor, tipoCalculo);
		}
		
		if(iCalculadora == null){
			return BigDecimal.ZERO;
		}
		return iCalculadora.calcular(valor, tipoCalculo);
	}
	
	public void setDistancia(ICalculadora iCalculadora){
		this.iCalculadora = iCalculadora;
	}
}
