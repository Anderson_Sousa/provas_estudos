package com.calculo.impl.conversao.distancia;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Centimetro implements ICalculadora {

	
	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		return valor[0].divide(BigDecimal.valueOf(100000l));
	}

}
