package com.calculo.impl.conversao.peso;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Miligrama implements ICalculadora {


	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		return valor[0].multiply(BigDecimal.valueOf(1000l));
	}


}
