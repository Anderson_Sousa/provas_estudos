package com.calculo.impl.conversao.peso;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Kilograma implements ICalculadora {


	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		return valor[0].divide(BigDecimal.valueOf(1000l));
	}

	public static void main(String[] args) {
		BigDecimal valor [] ={BigDecimal.valueOf(2500l)};
		System.out.println(new Kilograma().calcular(valor, null));
	}

}
