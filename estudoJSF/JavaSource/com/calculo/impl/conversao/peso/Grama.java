package com.calculo.impl.conversao.peso;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Grama implements ICalculadora {


	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		return valor[0].divide(BigDecimal.valueOf(10l));
	}
	public static void main(String[] args) {
		BigDecimal valor [] ={BigDecimal.valueOf(150l)};
		System.out.println(new Grama().calcular(valor, null));
	}

}
