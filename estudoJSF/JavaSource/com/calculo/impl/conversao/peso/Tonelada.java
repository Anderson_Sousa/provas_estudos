package com.calculo.impl.conversao.peso;

import java.math.BigDecimal;

import com.calculo.ICalculadora;

public class Tonelada implements ICalculadora {


	public BigDecimal calcular(BigDecimal[] valor, Integer tipoCalculo) {
		return valor[0].divide(BigDecimal.valueOf(1000000l));
	}

	public static void main(String[] args) {
		BigDecimal valor [] ={BigDecimal.valueOf(1000l)};
		System.out.println(new Tonelada().calcular(valor, null));
	}

}
