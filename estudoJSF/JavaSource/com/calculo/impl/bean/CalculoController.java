package com.calculo.impl.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlInputText;

import com.calculo.ICalculadora;
import com.calculo.impl.basico.Divisao;
import com.calculo.impl.basico.Multiplicacao;
import com.calculo.impl.basico.Percentual;
import com.calculo.impl.basico.RaizQuadrada;
import com.calculo.impl.basico.Soma;
import com.calculo.impl.basico.Subtracao;
import com.calculo.impl.util.TipoCalculoEnum;

public class CalculoController {

	private HtmlInputText inputNumero;
	private HtmlInputText inputNumeroOld;
	private String numeroClicado;
	private Integer sinal;
	private ICalculadora iCalculadora;
	private List<BigDecimal> valorLista = new ArrayList<BigDecimal>();
	private Boolean apague = Boolean.FALSE;
	
	/**
	 * 
	 */
	public CalculoController(){
		inputNumero = new HtmlInputText();
		inputNumero.setValue("");
	}
	/**
	 * 
	 */
	public void pegarNumero(){
		if(apague){
			inputNumero.setValue("");
			apague = Boolean.FALSE;
		}
		inputNumero.setValue(inputNumero.getValue() + "" + numeroClicado);
	}
	/**
	 * 
	 */
	public void exibirResultado(){
		valorLista.add(new BigDecimal(inputNumero.getValue() + ""));
		iCalculadora = new Soma(new Subtracao(new Divisao(new Multiplicacao(new Percentual(new RaizQuadrada(null))))));
		BigDecimal valor [] = new BigDecimal[valorLista.size()];
		int i = 0;
		for (BigDecimal valorCalculo : valorLista) {
			valor[i] = valorCalculo;
			i++;
		}
		valorLista.clear();
		if(sinal.equals(ICalculadora.RAIZ_QUADRADA)){
			inputNumeroOld.setValue(inputNumero.getValue());
		}else{
			inputNumeroOld.setValue(inputNumeroOld.getValue() + "" + inputNumero.getValue());
		}
		inputNumero.setValue(iCalculadora.calcular(valor, sinal));
	}
	/**
	 * 
	 */
	public void definirAcao(){
		valorLista.add(new BigDecimal(inputNumero.getValue() + ""));
		inputNumeroOld.setValue(inputNumero.getValue());
		inputNumeroOld.setValue(inputNumeroOld.getValue() + " " + TipoCalculoEnum.getInfo(sinal.intValue())  + " ");
		apague = Boolean.TRUE;
	}
	/**
	 * 
	 */
	public void limpar(){
		this.inputNumero.setValue("");
		this.inputNumeroOld.setValue("");
		this.numeroClicado = null;
		this.sinal = null;
		this.valorLista.clear();
	}
	
	public Integer getSinal() {
		return sinal;
	}
	public void setSinal(Integer sinal) {
		this.sinal = sinal;
	}
	public String getNumeroClicado() {
		return numeroClicado;
	}
	public void setNumeroClicado(String numeroClicado) {
		this.numeroClicado = numeroClicado;
	}
	public HtmlInputText getInputNumero() {
		return inputNumero;
	}
	public void setInputNumero(HtmlInputText inputNumero) {
		this.inputNumero = inputNumero;
	}
	public HtmlInputText getInputNumeroOld() {
		return inputNumeroOld;
	}
	public void setInputNumeroOld(HtmlInputText inputNumeroOld) {
		this.inputNumeroOld = inputNumeroOld;
	}
}
