package com.calculo;

import java.math.BigDecimal;


public interface ICalculadora {

	public static final Integer SOMA 			= 1;
	public static final Integer SUBTRACAO 		= 2;
	public static final Integer DIVISAO 		= 3;
	public static final Integer MULTIPLICACAO 	= 4;
	public static final Integer PERCENTUAL 		= 5;
	public static final Integer RAIZ_QUADRADA 	= 6;
	public static final Integer DISTANCIA 		= 7;
	public static final Integer PESO 			= 8;
	public static final Integer TEMPO			= 9;
	public static final Integer TEMPERATURA 	= 10;
	public static final Integer DIGITOS 		= 11;
	public static final Integer MOEDA 			= 12;
	public BigDecimal calcular(BigDecimal valor[], Integer tipoCalculo);

}
