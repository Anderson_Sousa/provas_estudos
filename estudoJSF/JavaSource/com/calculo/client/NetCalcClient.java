package com.calculo.client;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Scanner;

import com.calculo.ICalculadora;
import com.calculo.impl.basico.Divisao;
import com.calculo.impl.basico.Multiplicacao;
import com.calculo.impl.basico.Percentual;
import com.calculo.impl.basico.RaizQuadrada;
import com.calculo.impl.basico.Soma;
import com.calculo.impl.basico.Subtracao;
import com.calculo.impl.conversao.distancia.Centimetro;
import com.calculo.impl.conversao.distancia.Distancia;
import com.calculo.impl.conversao.distancia.Kilometro;
import com.calculo.impl.conversao.distancia.Metro;
import com.calculo.impl.conversao.distancia.Milimetro;

public class NetCalcClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		Scanner scanner = new Scanner(System.in);
		ICalculadora calculadora = null;
		
		continuar:
		while(scanner.hasNext()){
			String next = scanner.next();

			String arrayNumeros [] = next.split("\\,");
			if(!validar(arrayNumeros[0])){
				continue continuar;
			}
			BigDecimal valor [] = new BigDecimal [arrayNumeros.length - 1];
			for (int i = 0; i < (arrayNumeros.length - 1); i++) {
				if(!validar(arrayNumeros[i])){
					continue continuar;
				}
				valor [i] = new BigDecimal(arrayNumeros[i]);
			}
			String caracter = arrayNumeros[arrayNumeros.length - 1];
			
			if(caracter.equals("-")){
				calcular(calculadora, ICalculadora.SUBTRACAO, valor);
			}else if(caracter.equals("+")){
				calcular(calculadora, ICalculadora.SOMA, valor);
			}else if(caracter.equals("*")){
				calcular(calculadora, ICalculadora.MULTIPLICACAO, valor);
			}else if(caracter.equals("/")){
				calcular(calculadora, ICalculadora.DIVISAO, valor);
			}else if(caracter.equals("%")){
				calcular(calculadora, ICalculadora.PERCENTUAL, valor);
			}else if(caracter.equals("v")){
				calcular(calculadora, ICalculadora.RAIZ_QUADRADA, valor);
			}else if(caracter.equals("c")){
				calcular(calculadora, ICalculadora.DISTANCIA, valor);
			}
			else{
				if(caracter.length() > 1){
					System.out.println("Favor inserir somente um operador.");	
				}
				System.out.println("Favor informar um operador correto.");
				continue continuar;
			}
		}
		scanner.close();
	}
	
	public static void calcular(ICalculadora calculadora, Integer operador, BigDecimal valor []){

		Distancia distancia = new Distancia(new Centimetro());
		distancia.setDistancia(new Metro());
		distancia.setDistancia(new Kilometro());
		distancia.setDistancia(new Milimetro());
		calculadora = new Soma(new Subtracao(new Multiplicacao(new Divisao(new Percentual(new RaizQuadrada(distancia))))));
		System.out.println(calculadora.calcular(valor, operador));
	}
	
	public static boolean validar(String arrayNumeros){
		try{
			new BigInteger(arrayNumeros);
			return true;
		}catch(Exception e){
			System.out.println("Favor informar valor correto.");
			System.out.println("O formato do calculo deve seguir esta estrutura: valor, val..., val..., operador");
			return false;
		}
	}

}
